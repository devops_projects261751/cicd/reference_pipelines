# Reference pipelines - 

Objective of this repo is to have multiple .gitlab-ci.yml. If you commit change to on only one directory example 'production' then only actions defined in .gitlab-ci.yml of production directory is triggered. 

### Tested On - 
Azure cli 

Below is the project structure -

```
python_pipeline/
│
├── .gitlab-ci.yml
├── README.md
├── .gitignore
│
├── development/
│   │── dir_in_dev
│   │     └── file_in_dev
│   └── .gitlab-ci.yml
│
└── production/
    └── file_in_prod
    └── .gitlab-ci.yml
```

#### Aspects covered in this directory - 
* Manage multiple .gitlab-ci.yml 
* Trigges .gitlab-ci.yml depending on the changes made to specific directory. 

#### Further plans -
* Add conditional statement 
* Add rules 
* Add templates 
